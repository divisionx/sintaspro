<!--
SINTASPRO - Sistema integrado de asistencia al profesorado
* @link http://sintaspro.division-x.es/
* @author Alvaro Cuesta, http://division-x.es/
* @copyright 2012 Alvaro Cuesta
* @license http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
* @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other/una u otra)
* @version 0.01a
 -->
<?php
session_start();
include "inc/config.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SINTASPRO. Desarrollo por Alvaro Cuesta</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--Aqui empieza el fondo del header -->
<div id="header-bg">
	<!--Aqui empieza el contenido del header -->
	<div id="header">
		<a href="index.php"><img src="images/logo.gif" alt="Package" width="205" height="62" border="0" class="logo" title="Package" /></a>
		<!--Fondo del menu de login -->
		<div id="login-bg">
			<!--Area de login -->
			<div id="login-area">
				<form action="" method="post" name="Login" id="Login">
					<label>Acceso:</label>
					<input type="text" name="username" id="username" />
					<input type="password" name="pass" id="pass" />
					<input type="image" src="images/login-btn.gif" class="login-btn" alt="Login" title="Login" />
					<br class="spacer" />
				</form>
			</div>
			<!--Acaba el area de login-->
		</div>
		<!--Acaba el fondo del login -->
		<br class="spacer" />
	</div>
	<!--Acaba el contenido del header -->
</div>
<!--Acaba el fondo del header -->
<!--Empieza el fondo del menu -->
<div id="navegacion-bg">
	<!--Empieza la estructura de menu -->
	<div id="navegacion">
		<ul class="mainMenu">
			<li><a href="#" class="selectMenu" title="Inicio">Inicio</a></li>
			<li><a href="#" title="Buscar">Buscar</a></li>
			<li><a href="#" title="Panel">Panel</a></li>
			<li><a href="#" title="Soporte">Soporte</a></li>
		</ul>
		<br class="spacer" />
<!--		<ul class="subNav">
			<li class="noBg"><a href="#" title="Our Benefits">Our Benefits</a></li>
			<li><a href="#" title="What Our Future Plans">What Our Future Plans</a></li>
			<li><a href="#" title="Our Success">Our Success</a></li>
			<li><a href="#" title="Ratings">Ratings</a></li>
			<li><a href="#" title="Latest Blogs">Latest Blogs</a></li>
			<li><a href="#" title="News">News</a></li>
			<li><a href="#" title="Testimonials">Testimonials</a></li>
			<li><a href="#" title="Comments">Comments</a></li>
		</ul>-->
		<br class="spacer" />
	</div>
	<!--Acaba la estructura de menu -->
</div>
<!--Acaba el fondo de menu -->
<!--Empieza el fondo del contenido -->
<div id="contenido-bg">
	<!--Empieza el contenido -->
	<div id="contenido-part">
		<h2 class="contenido-hdr">Panel de desarrollo</h2>
		<!--Empieza la parte izquierda del contenido -->
		<div id="contenido-leftPart">
			<h2 class="faq-Hdr">Bugs y fallos</h2>
			<ul class="contenido-list">
				<li>De momento nada, vamos de puta madre ;)</li>
			</ul>
			<h2 class="moreIdeas-Hdr">Ideas a implementar</h2>
			<ul class="contenido-list noBottomPadding">
				<li>-Acceso.</li>
				<li>-Registro</li>
				<li>-IMPORTACION DE DATOS (!)</li>
			</ul>
		</div>
		<!--Acaba la parte izquierda del contenido -->
		<!--Empieza la parte derecha del contenido -->
		<div id="contenido-rightPart">
			<h2 class="moreInfo-Hdr">Documentacion</h2>
			<ul class="contenido-list noBottomPadding">
				<li><a href="http://www.webtaller.com/construccion/lenguajes/php/lecciones/leer-xml-simplexml-php.php">Leer XML desde PHP</a></li>
				<p class="moreInfo-Text">Mediante el uso de simpleXML</p>
				<li><a href="http://www.holamundo.es/lenguaje/php/articulos/leer-un-fichero-csv-variables-separadas-por-comas-en-php.html">Leer CSV desde PHP</a></li>
			</ul>
			<p class="moreInfo-Text">A lo bestia</p>
			<h2 class="searchUrl-Hdr">URL's relacionadas con el proyecto</h2>
			<ul class="contenido-list noBottomPadding">
				<li><a href="https://bitbucket.org/divisionx/sintaspro">Repositorio GIT</a></li>
			</ul>
		</div>
		<!--Acaba la parte derecha del contenido -->
		<br class="spacer" />
	</div>
	<!--Acaba el contenido -->
</div>
<!--Acaba el fondo del contenido -->

<!--Empieza el fondo del footer -->
<div id="footer-bg">
	<!--Empieza el menu del footer -->
	<div id="footer-menu">
		<ul class="footMenu">
			<li class="noDivider"><a href="#" title="Inicio">Inicio</a></li>
			<li><a href="#" title="Buscar">Buscar</a></li>
			<li><a href="#" title="Panel">Panel</a></li>
			<li><a href="#" title="Soporte">Soporte</a></li>
		</ul>
		<br class="spacer" />
		<p class="copyright">Copyright &copy; SINTASPRO - 2012 All Rights Reserved - Todos los derechos reservados</p>
		<p class="copyright topPad">Powered by <a href="http://division-x.es" target="_blank" title="Division-x">Alvaro "Division-x"</a></p>
	</div>
	<!--Acaba el menu del footer -->
</div>
<!--Acaba el fondo del footer -->
</body>
</html>
